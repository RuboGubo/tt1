import pygame
from pygame.locals import *
from graphics import Graphics
from move_objects import MoveObjects
import time

def main():
    app = App()
    move_objects = MoveObjects()

class App:
    def __init__(self):
        self._running = True
        self._display_surf = None
        self.graphics = Graphics()
        self.size = self.weight, self.height = 64, 40
        self.start_level = time.time()
        self.spacebar_last_pressed = time.time()
        self.loaded_level: list[tuple[bool]] = []
        self.load_level()
        self.interval = 0

    def load_level(self):
        with open("song.txt", "r") as file:
            self.interval = int(file.readline())
            for line in file:
                print(line, end="")
                self.loaded_level.append(tuple(line.strip("\n")))
        
    def on_event(self, event):
        if event.type == pygame.QUIT:
            self._running = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                new_time = time.time()
                print(f"Spacebar pressed time: {new_time - self.spacebar_last_pressed}")
                self.spacebar_last_pressed = new_time

    def on_cleanup(self):
        self.graphics.quit()
 
    def on_execute(self): 
        while self._running:
            for event in pygame.event.get():
                self.on_event(event)
        self.on_cleanup()
 
if __name__ == "__main__" :
    theApp = App()
    theApp.on_execute()




if __name__ == "__main__":
    main()
