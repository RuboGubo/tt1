import pygame as py
import random
import math

class Graphics:
    def __init__(self) -> None:
        self.screenWidth = 1200
        self.screenHeight = 800
        self.screen = py.display.set_mode((self.screenWidth, self.screenHeight))
        self.running = True

    def draw(self):
        self.screen.fill((255,255,255))
        court = self.floor(self.screenWidth, self.screenHeight)
        py.draw.polygon(self.screen, (108,147,92), court)
        py.draw.polygon(self.screen, (0,0,0), court, 1)
        for i in range(5):
            py.draw.polygon(self.screen, (0,0,0), self.shelfBracket(False, 100, ), 1)
            py.draw.polygon(self.screen, (0,0,0), self.shelfBracket(True, 200, 200), 1)
        py.display.flip()

        
    def floor(width, height):
        return [(100, height), (width/2, (width/2 - 100)*24), (width, height)]

    def quit(self) -> None:
        py.quit()

    def laight(how_laight: int):
        ...



